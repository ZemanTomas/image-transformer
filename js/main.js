function updateTextInput(val) {
    document.getElementById('quality').innerText = val;
}
function updateColr(val) {
    document.getElementById('colr').innerText = val;
}
function updateColg(val) {
    document.getElementById('colg').innerText = val;
}
function updateColb(val) {
    document.getElementById('colb').innerText = val;
}
function updateRec() {
    updateCrop(0,0,0,0);
    var imgsquishx = canvas.clientWidth / imageObj.width;
    var imgsquishy = canvas.clientHeight / imageObj.height;
    canvas.width = canvas.clientWidth;
    canvas.height = canvas.clientHeight;
    ctx = canvas.getContext('2d');
    ctx.strokeStyle = 'red';
    redraw(imageObj);
}

var canvas = null;
var ctx = null;
var rect = {};
var drag = false;
var imageObj = null;
var scaledw = 0;
var scaledh = 0;
function init() {
    if(canvas == null){
        canvas = document.getElementById('preview');
        canvas.width = canvas.clientWidth;
        canvas.height = canvas.clientHeight;
        canvas.oncontextmenu = function (e) {e.preventDefault();}
        ctx = canvas.getContext('2d');
        ctx.strokeStyle = 'red';
    }
    imageObj = new Image();
    imageObj.onload = function () { 
        drawScaled(this); 
        var previeww = this.naturalWidth;
        var previewh = this.naturalHeight;
        var resw = document.getElementById('resw'); resw.max = previeww * 16; resw.value = previeww;
        var resh = document.getElementById('resh'); resh.max = previewh * 16; resh.value = previewh;
        document.getElementById('rwh').innerText = "Real WxH: " + previeww + "x" + previewh;
    };
    imageObj.src = imgUrl;
    canvas.addEventListener('mousedown', mouseDown, false);
    canvas.addEventListener('mouseup', mouseUp, false);
    canvas.addEventListener('mousemove', mouseMove, false);
}

function drawScaled(imageObj){
    var scale = Math.min(canvas.clientWidth / imageObj.width, canvas.clientHeight / imageObj.height);
    scaledw = Math.floor(imageObj.width * scale);
    scaledh = Math.floor(imageObj.height * scale);
    document.getElementById('wh').innerText = "Display WxH: " + scaledw + "x" + scaledh;
    //var x = (canvas.clientWidth / 2) - (imageObj.width / 2) * scale;
    //var y = (canvas.clientHeight / 2) - (imageObj.height / 2) * scale;
    ctx.drawImage(imageObj, 0, 0, scaledw, scaledh);
}

function mouseDown(e) {
    relx = e.pageX - this.offsetLeft;
    rely = e.pageY - this.offsetTop;
    redraw(imageObj);
    if(relx < 0 || relx > scaledw || rely < 0 || rely > scaledh){
        updateCrop(0,0,0,0);
        return;
    }
    updateCrop(relx,rely,0,0);
    rect.startX = relx;
    rect.startY = rely;
    drag = true;
}

function mouseUp() { drag = false; }

function mouseMove(e) {
    if (drag) {
        redraw(imageObj);
        if(rect.startX > 0 && rect.startX < scaledw && rect.startY > 0 && rect.startY < scaledh){
            relx = e.pageX - this.offsetLeft;
            rely = e.pageY - this.offsetTop;
            if(relx < 0){relx = 0;}
            if(rely < 0){rely = 0;}
            if(relx > scaledw){relx = scaledw;}
            if(rely > scaledh){rely = scaledh;}
            rect.w = relx - rect.startX;
            rect.h = rely - rect.startY;
            //Add right click
            var rightclick;
            if(e.buttons){rightclick = (e.buttons == 2);}else if(e.which){rightclick = (e.which == 3);}
            if(rightclick){
                
                if(rect.w < 0){rect.w = rect.startX - relx;rect.startX = relx;}
                if(rect.h < 0){rect.h = rect.startY - rely;rect.startY = rely;}
            }
            ctx.strokeRect(rect.startX, rect.startY, rect.w, rect.h);
            updateCrop(rect.startX, rect.startY, rect.w, rect.h);
        }
    }
}

function redraw(imageObj){
    //ctx.clearRect(0, 0, canvas.clientWidth, canvas.clientHeight);//Approximately 0.019 ms on 573x789(1148x789) canvas
    ctx.clearRect(0, 0, scaledw+1, scaledh+1);//Approximately 0.012 ms on 573x789(1148x789) canvas
    drawScaled(imageObj);
}

function updateCrop(x,y,w,h){
    var valx = document.getElementById('cropx');
    var valy = document.getElementById('cropy');
    var valw = document.getElementById('cropw');
    var valh = document.getElementById('croph');
    var cropbtn = document.getElementById('cropbtn')
    if(w < 0){x+=w;w=Math.abs(w);}
    if(h < 0){y+=h;h=Math.abs(h);}
    var scale = Math.min(canvas.clientWidth / imageObj.width, canvas.clientHeight / imageObj.height);
    w = Math.floor(w/scale);h = Math.floor(h/scale);
    x = Math.floor(x/scale);y = Math.floor(y/scale);
    valx.max = imageObj.naturalWidth;
    valy.max = imageObj.naturalHeight;
    valx.value = x;
    valy.value = y;
    valw.max = valx.max - valx.value;
    valh.max = valy.max - valy.value;
    valw.value = w;
    valh.value = h;
    cropbtn.innerText="Cut "+valw.value+"x"+valh.value+" at "+valx.value+","+valy.value;
}

function menuChange(){
    var menuSelect = document.getElementById("menuselect");
    var menuDivs = document.getElementsByClassName("menuitem");
    for(var i = 0; i < menuDivs.length; ++i){
        if("menuitem"+menuSelect.value === menuDivs[i].id || menuDivs[i].id === "menuitemquality"){
            menuDivs[i].style.display = "block";
        }else{
            menuDivs[i].style.display = "none";
        }
    }
    
}