<?php
//Global
$target_dir = "img/";
if(isset($_GET["dir"])){$target_dir.=$_GET["dir"]."/";}
?>
<!DOCTYPE html>
<html>
<head>
<?php getHeader(); ?>
</head>
<body onload="init()" onresize="updateRec()">
<?php getBody(); ?>
</body>
</html>


<?php
function getMenuResult(){
	global $target_dir;
	//Check
	if(isset($_POST["submit"],$_FILES["file"]["name"])) {
		$target_file = $target_dir . basename($_FILES["file"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		$check = getimagesize($_FILES["file"]["tmp_name"]);
		if($check !== false) {
		    $uploadOk = 1;
		} else {
		    echo "File is not an image.<br>";
		    $uploadOk = 0;
		}
		// Check if directory exists
		if (!file_exists($target_dir)){
			if(!mkdir($target_dir, 0774, true)){
				echo "Unable to create directory";
				$uploadOk = 0;
			}
		}
		// Check if file already exists
		if (file_exists($target_file)) {
		    echo "Sorry, file already exists.<br>";
		    $uploadOk = 0;
		}
		// Check file size
		if ($_FILES["file"]["size"] > 40000000) {
		    echo "Sorry, your file is too large. 40 MB Maximum.<br>";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if(!in_array(strtolower($imageFileType),array("jpg","jpeg","bmp","png","ppm"))) {
		    echo "Sorry, only JPG, JPEG, BMP, PNG or PPM files are allowed.<br>";
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    echo "Sorry, your file was not uploaded.<br>";
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
		        echo "The file ". basename( $_FILES["file"]["name"]). " has been uploaded.<br>";
		    } else {
		        echo "Sorry, there was an error uploading your file.<br>";
		    }
		}
	}
	//Transform
	if(isset($_POST["transform"],$_GET["img"])){
		if(checkTransform($_POST["transform"])){
			//Construct parameters
			$transform = $_POST["transform"];
			$quality = $_POST["quality"];
			if($transform == "crop"){$transform .= " ".$_POST["cropw"]." ".$_POST["croph"]." ".$_POST["cropx"]." ".$_POST["cropy"];}
			if($transform == "col"){$transform .= " ".$_POST["colr"]." ".$_POST["colg"]." ".$_POST["colb"];}
			if($transform == "con"){$transform .= " ".$_POST["con"];}
			if($transform == "res"){$transform .= " ".$_POST["resw"]." ".$_POST["resh"];}
			if($transform == "matrix"){$transform .= " ".getMatrixString();}
			//Input filename
			$inputFileName=$target_dir.$_GET["img"];
			//Output filename
			if(isset($_POST["newfilename"],$_POST["newfileext"]) && $_POST["newfilename"] != "" && $_POST["transform"] == "copy"){
				$outputFileName=$target_dir.$_POST["newfilename"].$_POST["newfileext"];
			}else{
				$outputFileName=$target_dir.$_GET["img"];
			}
			//Command
			$cmd = "./transform -i ".$inputFileName." -o ".$outputFileName." -t \"".str_replace(",", " ", $transform)."\" -q ".$quality;
			$exitnum = 0;
			$time = microtime(true);
			exec($cmd,$output, $exitnum);
			$time = microtime(true) - $time;
			//Print to error log (or not)
			error_log($cmd);
			if($exitnum != 0){
				echo "Error<br>".print_r($output,true)."<br>";
			}else{
				echo "Done transformations in ".round($time*1000.0,2)."ms:<br>".$transform."<br>";
			}
		}
		else {echo "Error<br>";}
	}
	//Delete
	if(isset($_POST["del"])) {
			if(unlink($target_dir.$_POST["del"])){echo "Deleted successfully.<br>";}
	}
}

function getMenuListDir(){
	global $target_dir;
	echo "IMAGES:<button onclick=\"window.open('";
	getImgUrl();
	echo "','_blank')\">Show selected (new tab)</button><br><div class=\"divdir\"><form method=\"post\" enctype=\"multipart/form-data\">";
	if(file_exists($target_dir)){
		$row = exec('ls  '.$target_dir,$output,$error);
		$dir = "";
		if(isset($_GET["dir"])){$dir = "&dir=".$_GET["dir"];}
		while(list(,$row) = each($output)){if(is_dir($target_dir.$row)){continue;}
			if(isset($_GET["img"]) && ($row == $_GET["img"])){$sel = " (SELECTED) ";}else{$sel = "";}
			echo "<button class=\"delBtn\" type=\"submit\" name=\"del\" value=\"$row\" action=\"index.php\">X</button>
			<a href=\"index.php?img=$row$dir\">$row</a>$sel<br>";
		}
		if($error){
			echo "Error : $error<br>";
			exit();
		}
	}
	echo "</form></div>";
}

function getImgUrl(){
	global $target_dir;
	if(isset($_GET["img"]) && file_exists(($file = $target_dir.$_GET["img"]))){
		echo $file."?rnd=".md5(filemtime($file));
	}
}

function checkTransform($string){
		if($string == ""){return true;}//Yes, this is needed
		if($string == "rl"){return true;}
		if($string == "rr"){return true;}
		if($string == "fh"){return true;}
		if($string == "fv"){return true;}
		if($string == "crop"){return (isset($_POST["cropw"],$_POST["croph"],$_POST["cropx"],$_POST["cropy"]));}
		if($string == "col"){return (isset($_POST["colr"],$_POST["colg"],$_POST["colb"]));}
		if($string == "con"){return isset($_POST["con"]);}
		if($string == "res"){return (isset($_POST["resw"],$_POST["resh"]));}
		if($string == "gauss"){return true;}
		if($string == "matrix"){
			return 	isset($_POST["mat00"],$_POST["mat01"],$_POST["mat02"],
						  $_POST["mat10"],$_POST["mat11"],$_POST["mat12"],
						  $_POST["mat20"],$_POST["mat21"],$_POST["mat22"]);
		}
		if($string == "copy"){return true;}
		return false;
}

function getMatrixString(){
	return 	$_POST["mat00"]." ".$_POST["mat01"]." ".$_POST["mat02"]." ".
			$_POST["mat10"]." ".$_POST["mat11"]." ".$_POST["mat12"]." ".
			$_POST["mat20"]." ".$_POST["mat21"]." ".$_POST["mat22"];
}

function getSelectExt($ext){
	if(isset($_GET["img"]) && $_GET["img"] != ""){
		if($ext == pathinfo($_GET["img"], PATHINFO_EXTENSION)){
			echo "selected=\"$ext\"";
		}
	}
}

function getget(){
	$gets = array("img", "dir");
	$numpar = 0;
	$set = false;
	foreach($gets as $get){if(isset($_GET[$get])){$set = true;}}
	if($set){echo "?";}
	foreach($gets as $get){if(isset($_GET[$get])){if($numpar != 0){echo "&";}echo "$get=".$_GET[$get];$numpar += 1;}}
}

function getHeader(){?>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<script>var imgUrl = "<?= getImgUrl() ?>" ; </script>
	<script src="js/main.js"></script>
	<title>ImageTransformer <?=`./transform --version 2>&1`?></title>
<?php }

function getBody(){?>
<div class="flexm">
	<div class="divform">
		<?php getMenu(); ?>
	</div>
	<div class="divimg">
		<?php getImage(); ?>
	</div>
</div>
<?php }

function getMenu(){?>
<div class="flexmenu" id="flexmenuheader">
	<?php getMenuHeader(); ?>
</div>
<hr>
<div class="flexmenu" id="flexmenubody">
	<form action="index.php<?php getget(); ?>" method="post" id="transform" enctype="multipart/form-data">
		<?php getMenuBody(); ?>
	</form>
</div>
<hr>
<div class="flexmenu" id="flexmenuresult">
	Log:<br>
	<?php getMenuResult(); ?>
</div>
<hr>
<div class="flexmenu" id="flexmenulistdir">
	<?php getMenuListDir(); ?>
</div>
<?php }

function getImage(){
	global $target_dir;
	if(isset($_GET["img"]) && file_exists(($file = $target_dir.$_GET["img"]))){
		echo "<canvas id=\"preview\" >";
	}}

function getMenuHeader(){?>
	<form action="index.php<?php getget(); ?>" method="post" enctype="multipart/form-data">
			<div id="wh">Display WxH:</div>
			<div id="rwh">Real WxH:</div>
			Select image to upload:
			<br>
			<input type="file" name="file" id="file">
			<br>
			<input type="submit" value="Upload Image" name="submit"> 
			<br>
	</form>
<?php }


function getMenuBody(){?>
	<select id="menuselect" onchange="menuChange()">
		<option value="0" selected="selected" id="menu0"> Please select transformation </option>
		<option value="1" id="menu1">Rotation/Flip</option>
		<option value="2" id="menu2">Cut</option>
		<option value="3" id="menu3">Resize</option>
		<option value="4" id="menu4">Custom 3x3 filter</option>
		<option value="5" id="menu5">Gaussian blur 3x3</option>
		<option value="6" id="menu6">Change contrast</option>
		<option value="7" id="menu7">Change colours</option>
		<option value="8" id="menu8">Copy/Recode</option>
	</select>
		<div class="menuitem" id="menuitem1">

			<button type="submit" value="rl" name="transform">Rotate left</button>
			<button type="submit" value="rr" name="transform">Rotate right</button>
			<br>
			<button type="submit" value="fh" name="transform">Flip horizontally</button>
			<button type="submit" value="fv" name="transform">Flip vertically</button>

		</div>
		<div class="menuitem" id="menuitem2">

			<button type="submit" value="crop" name="transform" id="cropbtn">Cut 0x0 at 0,0</button>
			<input class="cropinpt" type="number" min="0" max="0" value="0" name="cropw" for="transform" id="cropw" onchange="updateRec()">
			<input class="cropinpt" type="number" min="0" max="0" value="0" name="croph" for="transform" id="croph" onchange="updateRec()">
			<input class="cropinpt" type="number" min="0" max="0" value="0" name="cropx" for="transform" id="cropx" onchange="updateRec()">
			<input class="cropinpt" type="number" min="0" max="0" value="0" name="cropy" for="transform" id="cropy" onchange="updateRec()">

		</div>
		<div class="menuitem" id="menuitem3">

			W:<input class="resinpt" type="number" min="1" max="1" value="1" name="resw" for="transform" id="resw">
			H:<input class="resinpt" type="number" min="1" max="1" value="1" name="resh" for="transform" id="resh">
			<button type="submit" value="res" name="transform">Resize</button>

		</div>
		<div class="menuitem" id="menuitem4">

			<input class="matrixinpt" type="number" min="-10" max="10" value="0" step=0.01 name="mat00" for="transform">
			<input class="matrixinpt" type="number" min="-10" max="10" value="0" step=0.01 name="mat01" for="transform">
			<input class="matrixinpt" type="number" min="-10" max="10" value="0" step=0.01 name="mat02" for="transform">
			<br>
			<input class="matrixinpt" type="number" min="-10" max="10" value="0" step=0.01 name="mat10" for="transform">
			<input class="matrixinpt" type="number" min="-10" max="10" value="1" step=0.01 name="mat11" for="transform">
			<input class="matrixinpt" type="number" min="-10" max="10" value="0" step=0.01 name="mat12" for="transform">
			<br>
			<input class="matrixinpt" type="number" min="-10" max="10" value="0" step=0.01 name="mat20" for="transform">
			<input class="matrixinpt" type="number" min="-10" max="10" value="0" step=0.01 name="mat21" for="transform">
			<input class="matrixinpt" type="number" min="-10" max="10" value="0" step=0.01 name="mat22" for="transform">
			<br>
			<button type="submit" value="matrix" name="transform">Apply matrix:</button>

		</div>
		<div class="menuitem" id="menuitem5">

			<button type="submit" value="gauss" name="transform">Gaussian blur</button>

		</div>
		<div class="menuitem" id="menuitem6">

			<input type="number" class="coninpt" min="-255" max="255" value="0" name="con" for="transform">
			<button type="submit" value="con" name="transform">Change contrast</button>

		</div>
		<div class="menuitem" id="menuitem7">

			<br>
			R:<input type="range" name="colr" class="colr wkcol" for="transform" min=-1.0 max=1.0 value=0.0 step=0.01 onchange="updateColr(this.value);">
			<label id="colr">0</label>
			<br>
			G:<input type="range" name="colg" class="colg wkcol" for="transform" min=-1.0 max=1.0 value=0.0 step=0.01 onchange="updateColg(this.value);">
			<label id="colg">0</label>
			<br>
			B:<input type="range" name="colb" class="colb wkcol" for="transform" min=-1.0 max=1.0 value=0.0 step=0.01 onchange="updateColb(this.value);">
			<label id="colb">0</label>
			<button type="submit" value="col" name="transform">Update colours</button>
		
		</div>
		<div class="menuitem" id="menuitem8">

			Copy/Recode:
				<input class="newfilenameinpt" type="text" name="newfilename" for="transform" value="<?php if(isset($_GET["img"])){echo pathinfo($_GET["img"],PATHINFO_FILENAME);} ?>">
				<select class="newfileextinpt" name="newfileext" for="transform">
					<option value=".jpeg" <?= getSelectExt("jpeg") ?>>.jpeg</option>
					<option value=".jpg" <?= getSelectExt("jpg") ?>>.jpg</option>
					<option value=".bmp" <?= getSelectExt("bmp") ?>>.bmp</option>
					<option value=".png" <?= getSelectExt("png") ?>>.png</option>
					<option value=".webp" <?= getSelectExt("webp") ?>>.webp</option>
				</select>
				<button type="submit" value="copy" name="transform">Copy/Recode</button>

		</div>
		<div class="menuitem" id="menuitemquality" style="display: block">
			<?php getMenuForm(); ?>
		</div>
<?php }

function getMenuForm(){?>
			Quality:<br>
			<input type="range" name="quality" for="transform" min=1 max=100 value=100 onchange="updateTextInput(this.value);">
			<label id="quality">100</label>
			<br>
<?php }
