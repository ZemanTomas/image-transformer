#include "transform.hpp"
void Transform::res(int32_t nw, int32_t nh){
	//Init
	uint8_t *temp = (uint8_t*)malloc(sizeof(uint8_t)*nw*nh*3);
        uint8_t buf[3];
	//Transform
        for(int32_t y = 0; y < nh; ++y){
                float v = (float)y / (float)(nh-1);
                int32_t rc = y*nw*3;
                for(int32_t x = 0; x < nw; ++x){
                        float u = (float)x / (float)(nw-1);
                        int32_t rr = x*3;
                        bicubic(u,v,buf);
                        temp[rc + rr + 0] = buf[0];
                        temp[rc + rr + 1] = buf[1];
                        temp[rc + rr + 2] = buf[2];
                }
        }
	//Clean
	free(image.image);
	image.image = temp;
	image.width = nw;
	image.height= nh;
	image.stride= nw*3;
}

void Transform::bicubic(float u, float v, uint8_t *buf){
	int32_t w = image.width, h = image.height;
	float x = (u*w) - 0.5f;
        int32_t xint = (int32_t)x;
        float xfract = x-floor(x);
        float y = (v*h) - 0.5f;
        int32_t yint = (int32_t)y;
        float yfract = y - floor(y);
        uint8_t p00[3],p10[3],p20[3],p30[3],
                p01[3],p11[3],p21[3],p31[3],
                p02[3],p12[3],p22[3],p32[3],
                p03[3],p13[3],p23[3],p33[3];
                getpix(xint - 1, yint - 1,p00);
                getpix(xint + 0, yint - 1,p10);
                getpix(xint + 1, yint - 1,p20);
                getpix(xint + 2, yint - 1,p30);
                getpix(xint - 1, yint + 0,p01);
                getpix(xint + 0, yint + 0,p11);
                getpix(xint + 1, yint + 0,p21);
                getpix(xint + 2, yint + 0,p31);
                getpix(xint - 1, yint + 1,p02);
                getpix(xint + 0, yint + 1,p12);
                getpix(xint + 1, yint + 1,p22);
                getpix(xint + 2, yint + 1,p32);
                getpix(xint - 1, yint + 2,p03);
                getpix(xint + 0, yint + 2,p13);
                getpix(xint + 1, yint + 2,p23);
                getpix(xint + 2, yint + 2,p33);

                for (int32_t i = 0; i < 3; ++i) {
                        float col0 = herm(p00[i], p10[i], p20[i], p30[i], xfract);
                        float col1 = herm(p01[i], p11[i], p21[i], p31[i], xfract);
                        float col2 = herm(p02[i], p12[i], p22[i], p32[i], xfract);
                        float col3 = herm(p03[i], p13[i], p23[i], p33[i], xfract);
                        buf[i] = (uint8_t)max(0.0f,min(255.0f,herm(col0,col1,col2,col3,yfract)));
                }

}

float Transform::herm(float a,float b,float c,float d, float t){
	return  ((-a/2.0f)+((3.0f*b)/2.0f)-((3.0f*c)/2.0f)+(d/2.0f))*t*t*t+
                (a-((5.0f*b)/2.0f)+(2.0f*c)-(d/2.0f))*t*t+
                ((-a/2.0f)+(c/2.0f))*t+
                b;
}