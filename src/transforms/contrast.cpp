#include "transform.hpp"
void Transform::con(int32_t con){
        //Init
	int32_t w = image.width, h = image.height;
        con = min(255, max(-255, con));
        float f = (float)(259 * (con + 255)) / (float)(255 * (259 - con));
        //Init LT
	uint8_t LT[255];
	for(int32_t i = 0; i < 255; ++i){
		LT[i]=(uint8_t)min(255,max(0,(int32_t)(f*(i-128))+128));
	}
        //Transform
        for(int32_t i = 0; i < h; ++i){
                	int32_t br = i*w*3;
                for(int32_t j = 0; j < w; ++j){
                        int32_t bc = j*3;
			image.image[br + bc + 0] = LT[image.image[br + bc + 0]];
			image.image[br + bc + 1] = LT[image.image[br + bc + 1]];
			image.image[br + bc + 2] = LT[image.image[br + bc + 2]];
        }}
        //Clean
}