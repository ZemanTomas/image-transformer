#include "transform.hpp"
void Transform::col(float r, float g, float b){
	//Init
	int32_t w = image.width, h = image.height;
        //Init LT
	uint8_t LTR[255],LTG[255],LTB[255];
	for(int32_t i = 0; i < 255; ++i){
		LTR[i]=(uint8_t)min(255,max(0,(int32_t)(r*(i-128))+128));
		LTG[i]=(uint8_t)min(255,max(0,(int32_t)(g*(i-128))+128));
		LTB[i]=(uint8_t)min(255,max(0,(int32_t)(b*(i-128))+128));
	}
	//Transform
	for(int32_t i = 0; i < h; ++i){
                	int32_t br = i*w*3;
                for(int32_t j =0; j < w; ++j){
                        int32_t bc = j*3;
			image.image[br + bc + 0] = LTR[image.image[br + bc + 0]];
			image.image[br + bc + 1] = LTG[image.image[br + bc + 1]];
			image.image[br + bc + 2] = LTB[image.image[br + bc + 2]];
                }
        }
	//Clean
}