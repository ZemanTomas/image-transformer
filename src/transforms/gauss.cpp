#include "transform.hpp"
void Transform::gauss1(){
	//Init
	int32_t w = image.width, h = image.height;
	uint8_t *temp = (uint8_t*)malloc(sizeof(uint8_t)*image.height*image.stride);
    uint8_t p00[3],p01[3],p02[3],p10[3],p11[3],p12[3],p20[3],p21[3],p22[3];
    //Transform
	for(int32_t i = 0; i < h; ++i){
        int32_t mc = i*image.stride;
        for(int32_t j =0; j < w; ++j){
            int32_t mr = j*3;
            getpix(j-1,i-1,p00);getpix(j+0,i-1,p01);getpix(j+1,i-1,p02);
            getpix(j-1,i+0,p10);getpix(j+0,i+0,p11);getpix(j+1,i+0,p12);
            getpix(j-1,i+1,p20);getpix(j+0,i+1,p21);getpix(j+1,i+1,p22);
            for(int32_t c = 0; c < 3; ++c){
                temp[mc + mr + c] = (uint8_t)(  (float)p00[c]*0.077847+(float)p01[c]*0.123317+(float)p02[c]*0.077847+
                                                (float)p10[c]*0.123317+(float)p11[c]*0.195346+(float)p12[c]*0.123317+
                                                (float)p20[c]*0.077847+(float)p21[c]*0.123317+(float)p22[c]*0.077847);
            }
        }
        }
	//Clean
	free(image.image);
	image.image = temp;
}

void Transform::gauss1p(){
	//Init
	int32_t w = image.width, h = image.height;
	uint8_t *temp = (uint8_t*)malloc(sizeof(uint8_t)*image.height*image.stride);
    uint8_t p00[3],p01[3],p02[3],p10[3],p11[3],p12[3],p20[3],p21[3],p22[3];
    //LUT
    float LT1[256],LT2[256],LT3[256];
	for(int32_t i = 0; i < 256; ++i){
		LT1[i]=(float)i * 0.077847;
		LT2[i]=(float)i * 0.123317;
		LT3[i]=(float)i * 0.195346;
	}
    //Transform
	for(int32_t i = 0; i < h; ++i){
        int32_t mc = i*image.stride;
        for(int32_t j =0; j < w; ++j){
            int32_t mr = j*3;
            getpix(j-1,i-1,p00);getpix(j+0,i-1,p01);getpix(j+1,i-1,p02);
            getpix(j-1,i+0,p10);getpix(j+0,i+0,p11);getpix(j+1,i+0,p12);
            getpix(j-1,i+1,p20);getpix(j+0,i+1,p21);getpix(j+1,i+1,p22);
            for(int32_t c = 0; c < 3; ++c){
                temp[mc + mr + c] = (uint8_t)(  LT1[p00[c]]+LT2[p01[c]]+LT1[p02[c]]+
                                                LT2[p10[c]]+LT3[p11[c]]+LT2[p12[c]]+
                                                LT1[p20[c]]+LT2[p21[c]]+LT1[p22[c]]);
            }
        }
        }
	//Clean
	free(image.image);
	image.image = temp;
}

void Transform::gauss2(){
	//Init
	int32_t w = image.width, h = image.height;
	uint8_t *temp = (uint8_t*)malloc(sizeof(uint8_t)*image.height*image.stride);
    uint8_t p0[3],p1[3],p2[3];
    //LUT
    float LT1[256],LT2[256];
	for(int32_t i = 0; i < 256; ++i){
		LT1[i]=(float)i * 0.27901;
		LT2[i]=(float)i * 0.44198;
	}
    //Transform horizontal
    int32_t mc = 0;
	for(int32_t j = 0; j < h; ++j){
        int32_t mr = 0;
        for(int32_t i =0; i < w; ++i){
            getpix(i-1,j,p0);getpix(i,j,p1);getpix(i+1,j,p2);
            for(int32_t c = 0; c < 3; ++c){
                temp[mc + mr + c] = (uint8_t)(LT1[p0[c]]+LT2[p1[c]]+LT1[p2[c]]);
            }
            mr+=3;
        }
        mc+=image.stride;
    }
    //Swap
    swap(temp,image.image);
    //Transform vertical
    int32_t mr = 0;
	for(int32_t i = 0; i < w; ++i){
        int32_t mc = 0;
        for(int32_t j =0; j < h; ++j){
            getpix(i,j-1,p0);getpix(i,j,p1);getpix(i,j-1,p2);
            for(int32_t c = 0; c < 3; ++c){
                temp[mc + mr + c] = (uint8_t)(LT1[p0[c]]+LT2[p1[c]]+LT1[p2[c]]);
            }
            mc+=image.stride;
        }
        mr+=3;
    }
	//Clean
	free(image.image);
	image.image = temp;
}
void Transform::gauss3(){
    //3*BoxBlur
	uint8_t *temp = (uint8_t*)malloc(sizeof(uint8_t)*image.height*image.stride);
    gaussBoxH(temp);
    swap(temp,image.image);
    gaussBoxT(temp);
    swap(temp,image.image);
    gaussBoxH(temp);
    swap(temp,image.image);
    gaussBoxT(temp);
    swap(temp,image.image);
    gaussBoxH(temp);
    swap(temp,image.image);
    gaussBoxT(temp);
	free(image.image);
	image.image = temp;
}
void Transform::gaussBoxH(uint8_t *temp){
    //Init
	int32_t w = image.width, h = image.height;
    uint8_t p0[3],p1[3],p2[3];
    //Transform
    int32_t mc = 0;
	for(int32_t j = 0; j < h; ++j){
        int32_t mr = 0;
        for(int32_t i =0; i < w; ++i){
            getpix(i-1,j,p0);getpix(i,j,p1);getpix(i+1,j,p2);
            for(int32_t c = 0; c < 3; ++c){
                temp[mc + mr + c] = (uint8_t)((p0[c]+p1[c]+p2[c])/3);
            }
            mr+=3;
        }
        mc+=image.stride;
    }
}
void Transform::gaussBoxT(uint8_t *temp){
    //Init
	int32_t w = image.width, h = image.height;
    uint8_t p0[3],p1[3],p2[3];
    //Transform
    int32_t mr = 0;
	for(int32_t i = 0; i < w; ++i){
        int32_t mc = 0;
        for(int32_t j =0; j < h; ++j){
            getpix(i,j-1,p0);getpix(i,j,p1);getpix(i,j+1,p2);
            for(int32_t c = 0; c < 3; ++c){
                temp[mc + mr + c] = (uint8_t)((p0[c]+p1[c]+p2[c])/3);
            }
            mc+=image.stride;
        }
        mr+=3;
    }
}
void Transform::gauss4(){
    //3*BoxBlur+AC
	uint8_t *temp = (uint8_t*)malloc(sizeof(uint8_t)*image.height*image.stride);
    //LUT
    uint8_t LT[766];
	for(uint16_t i = 0; i < 766; ++i){LT[i]=(uint8_t)(i/3);}
    gaussBoxHA(temp,LT);
    swap(temp,image.image);
    gaussBoxTA(temp,LT);
    swap(temp,image.image);
    gaussBoxHA(temp,LT);
    swap(temp,image.image);
    gaussBoxTA(temp,LT);
    swap(temp,image.image);
    gaussBoxHA(temp,LT);
    swap(temp,image.image);
    gaussBoxTA(temp,LT);
	free(image.image);
	image.image = temp;
}
void Transform::gaussBoxHA(uint8_t *temp, uint8_t LT[766]){
    //Init
	int32_t w = image.width, h = image.height;
    uint8_t p0[3],p1[3],p2[3];
    uint16_t ac[3];
    //Transform
    int32_t mc = 0;
	for(int32_t j = 0; j < h; ++j){
        int32_t mr = 0;
        getpix(-1,j,p0);getpix(0,j,p1);
        for(int32_t c = 0; c < 3; ++c){ac[c] = (uint16_t)p0[c]+(uint16_t)p1[c];}
        for(int32_t i = 0; i < w; ++i){
            getpix(i-1,j,p0);getpix(i+1,j,p2);
            for(int32_t c = 0; c < 3; ++c){
                ac[c] += p2[c];
                temp[mc + mr + c] = LT[ac[c]];
                ac[c] -= p0[c];
            }
            mr+=3;
        }
        mc+=image.stride;
    }
}
void Transform::gaussBoxTA(uint8_t *temp, uint8_t LT[766]){
    //Init
	int32_t w = image.width, h = image.height;
    uint8_t p0[3],p1[3],p2[3];
    uint16_t ac[3];
    //Transform
    int32_t mr = 0;
	for(int32_t i = 0; i < w; ++i){
        int32_t mc = 0;
        getpix(i,-1,p0);getpix(i,0,p1);
        for(int32_t c = 0; c < 3; ++c){ac[c] = (uint16_t)p0[c]+(uint16_t)p1[c];}
        for(int32_t j = 0; j < h; ++j){
            getpix(i,j-1,p0);getpix(i,j+1,p2);
            for(int32_t c = 0; c < 3; ++c){
                ac[c] += p2[c];
                temp[mc + mr + c] = LT[ac[c]];
                ac[c] -= p0[c];
            }
            mc+=image.stride;
        }
        mr+=3;
    }
}
__attribute__ ((hot))
void Transform::gauss5(){
	//Init
	const uint32_t w = image.width, h = image.height,linesize = sizeof(uint8_t)*image.stride;
    const uint32_t w3 = w*3;
    uint32_t c = 0;
    uint8_t *temp  = (uint8_t*)malloc(linesize*4);
	uint8_t *temp0 = temp;
	uint8_t *temp1 = temp0+linesize;
	uint8_t *temp2 = temp1+linesize;
	uint8_t *tempv = temp2+linesize;
    uint8_t *imgout = image.image;
    //LUT
    uint16_t LT1[256],LT2[256],LT3[256];
	for(uint32_t i = 0; i < 256; ++i){
		LT1[i]=(uint16_t)((i<<8) * 0.27901f);
		LT2[i]=(uint16_t)((i<<8) * 0.44198f);
		LT3[i]=(uint16_t)((i<<8) * 0.27901f);
	}
    //Transform horizontal
    //Top
    copy(imgout,imgout+linesize*3,temp);
    //BlurH0
    for(uint32_t i =0; i < w3; i+=3){
        for(c = 0; c < 3; ++c){tempv[i + c] = (uint8_t)((LT1[temp0[i+c]]+LT2[temp0[i+c]]+LT3[temp1[i+c]])>>8);}
    }
    //BlurV0
    for(c = 0; c < 3; ++c){imgout[c] = (uint8_t)((LT1[tempv[c]]+LT2[tempv[c]]+LT3[tempv[c+3]])>>8);}
    for(uint32_t i =3; i < w3-3; i+=3){
        for(c = 0; c < 3; ++c){imgout[i + c] = (uint8_t)((LT1[tempv[i+c-3]]+LT2[tempv[i+c]]+LT3[tempv[i+c+3]])>>8);}
    }
    for(c = 0; c < 3; ++c){imgout[w3 + c] = (uint8_t)((LT1[tempv[w3+c-3]]+LT2[tempv[w3+c]]+LT3[tempv[w3+c]])>>8);}
    //BlurH1
    for(uint32_t i =0; i < w3; i+=3){
        for(c = 0; c < 3; ++c){tempv[i + c] = (uint8_t)((LT1[temp0[i+c]]+LT2[temp1[i+c]]+LT3[temp2[i+c]])>>8);}
    }
    //BlurV1
    imgout += linesize;
    for(c = 0; c < 3; ++c){imgout[c] = (uint8_t)((LT1[tempv[c]]+LT2[tempv[c]]+LT3[tempv[c+3]])>>8);}
    for(uint32_t i =3; i < w3-3; i+=3){
        for(c = 0; c < 3; ++c){imgout[i + c] = (uint8_t)((LT1[tempv[i+c-3]]+LT2[tempv[i+c]]+LT3[tempv[i+c+3]])>>8);}
    }
    for(c = 0; c < 3; ++c){imgout[w3 + c] = (uint8_t)((LT1[tempv[w3+c-3]]+LT2[tempv[w3+c]]+LT3[tempv[w3+c]])>>8);}
    imgout += linesize;
    //Mid
    //Blur
	for(uint32_t j = 3; j < h-1; ++j){
        //Move
        swap(temp0,temp1);
        swap(temp1,temp2);
        copy(imgout+linesize,imgout+linesize*2,temp2);
        //BlurH
        for(uint32_t i =0; i < w3; i+=3){
            for(c = 0; c < 3; ++c){tempv[i + c] = (uint8_t)((LT1[temp0[i+c]]+LT2[temp1[i+c]]+LT3[temp2[i+c]])>>8);}
        }
        //BlurV
        for(c = 0; c < 3; ++c){imgout[c] = (uint8_t)((LT1[tempv[c]]+LT2[tempv[c]]+LT3[tempv[c+3]])>>8);}
        for(uint32_t i =3; i < w3-3; i+=3){
            for(c = 0; c < 3; ++c){
                imgout[i + c] = (uint8_t)((LT1[tempv[i+c-3]]+LT2[tempv[i+c]]+LT3[tempv[i+c+3]])>>8);
            }
        }
        for(c = 0; c < 3; ++c){imgout[w3 + c] = (uint8_t)((LT1[tempv[w3+c-3]]+LT2[tempv[w3+c]]+LT3[tempv[w3+c]])>>8);}
        imgout += linesize;
    }
    //Bot
    //BlurH+1
    for(uint32_t i = 0; i < w3; i+=3){
        for(c = 0; c < 3; ++c){tempv[i + c] = (uint8_t)((LT1[temp1[i+c]]+LT2[temp2[i+c]]+LT3[temp2[i+c]])>>8);}
    }
    //BlurV
    for(c = 0; c < 3; ++c){imgout[c] = (uint8_t)((LT1[tempv[c]]+LT2[tempv[c]]+LT3[tempv[c+3]])>>8);}
    for(uint32_t i =3; i < w3-3; i+=3){
        for(c = 0; c < 3; ++c){imgout[i + c] = (uint8_t)((LT1[tempv[i+c-3]]+LT2[tempv[i+c]]+LT3[tempv[i+c+3]])>>8);}
    }
    for(c = 0; c < 3; ++c){imgout[w3 + c] = (uint8_t)((LT1[tempv[w3+c-3]]+LT2[tempv[w3+c]]+LT3[tempv[w3+c]])>>8);}
	//Clean
	free(temp);
}
//Gauss 6 -> cache box version