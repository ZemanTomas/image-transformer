#include "transform.hpp"
void Transform::crop(int32_t new_w, int32_t new_h, int32_t x, int32_t y){
        //Init
	int32_t w = image.width, h = image.height;
 	new_w = min(new_w,w-x);
        new_h = min(new_h,h-y);
        uint8_t* temp = (uint8_t*)malloc(sizeof(uint8_t)*new_w*new_h*3);
        //Transform
        for(int32_t i = y,ni=0; i < y+new_h; ++i,++ni){
                for(int32_t j = x,nj=0; j < x+new_w; ++j,++nj){
                        int32_t tr = ni*new_w*3,
                                tc = nj*3,
                                br = i*w*3,
                                bc = j*3;
                        temp[tr + tc + 0] = image.image[br + bc + 0];
                        temp[tr + tc + 1] = image.image[br + bc + 1];
                        temp[tr + tc + 2] = image.image[br + bc + 2];
        }}
        //Clean
	free(image.image);
	image.image = temp;
	image.height = new_h;
	image.width = new_w;
        image.stride = image.width*3;
}