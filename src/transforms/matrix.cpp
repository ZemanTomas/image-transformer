#include "svd3.h"
#include "transform.hpp"
void Transform::matrix(float mat[9]){
	//Init
	int32_t w = image.width, h = image.height;
	uint8_t *temp = (uint8_t*)malloc(sizeof(uint8_t)*image.height*image.stride);
        float invsum =(float)(mat[0]+mat[1]+mat[2]+mat[3]+mat[4]+mat[5]+mat[6]+mat[7]+mat[8]);
        for(int32_t i = 0; i < 9 && (abs(invsum) > 0.099); ++i){mat[i] /= invsum;}
        uint8_t p00[3],p01[3],p02[3],p10[3],p11[3],p12[3],p20[3],p21[3],p22[3];
        //Transform
	for(int32_t i = 0; i < h; ++i){
                int32_t mc = i*image.stride;
                for(int32_t j =0; j < w; ++j){
                        int32_t mr = j*3;
                        getpix(j-1,i-1,p00);getpix(j+0,i-1,p01);getpix(j+1,i-1,p02);
                        getpix(j-1,i+0,p10);getpix(j+0,i+0,p11);getpix(j+1,i+0,p12);
                        getpix(j-1,i+1,p20);getpix(j+0,i+1,p21);getpix(j+1,i+1,p22);
                        for(int32_t c = 0; c < 3; ++c){
                                temp[mc + mr + c] = (uint8_t)(  (float)p00[c]*mat[0]+(float)p01[c]*mat[1]+(float)p02[c]*mat[2]+
                                                                (float)p10[c]*mat[3]+(float)p11[c]*mat[4]+(float)p12[c]*mat[5]+
                                                                (float)p20[c]*mat[6]+(float)p21[c]*mat[7]+(float)p22[c]*mat[8]);
                        }
                }
        }
	//Clean
	free(image.image);
	image.image = temp;
}

__attribute__ ((hot))
void Transform::matrix2(float mat[9]){
	//Init
	const uint32_t w = image.width, h = image.height,linesize = sizeof(uint8_t)*image.stride;
    const uint32_t w3 = w*3;
    uint32_t c = 0;
    uint8_t *temp  = (uint8_t*)malloc(linesize*3);
    uint8_t *temp0 = temp;
    uint8_t *temp1 = temp0+linesize;
    uint8_t *temp2 = temp1+linesize;
    uint8_t *imgout = image.image;
    //LUT
    float LT[9][256];
    float inv = 0;
    for(uint32_t j = 0; j < 9; ++j){inv += mat[j];}
    for(uint32_t j = 0; j < 9; ++j){
	for(uint32_t i = 0; i < 256; ++i){
		LT[j][i]=(i * mat[j])/inv;
	}
    }
    //Transform horizontal
    //Top
    copy(imgout,imgout+linesize*3,temp);
    //BlurV0
    for(c = 0; c < 3; ++c){imgout[c] = (uint8_t)((      LT[0][temp0[c]]+LT[1][temp0[c]]+LT[2][temp0[c+3]]+
                                                        LT[3][temp0[c]]+LT[4][temp0[c]]+LT[5][temp0[c+3]]+
                                                        LT[6][temp1[c]]+LT[7][temp1[c]]+LT[8][temp1[c+3]]));}
    for(uint32_t i =3; i < w3-3; i+=3){
        for(c = 0; c < 3; ++c){imgout[i + c] = (uint8_t)((      LT[0][temp0[i+c-3]]+LT[1][temp0[i+c]]+LT[2][temp0[i+c+3]]+
                                                                LT[3][temp0[i+c-3]]+LT[4][temp0[i+c]]+LT[5][temp0[i+c+3]]+
                                                                LT[6][temp1[i+c-3]]+LT[7][temp1[i+c]]+LT[8][temp1[i+c+3]]));}
    }
    for(c = 0; c < 3; ++c){imgout[w3 + c] = (uint8_t)(( LT[0][temp0[w3+c-3]]+LT[1][temp0[w3+c]]+LT[2][temp0[w3+c+3]]+
                                                        LT[3][temp0[w3+c-3]]+LT[4][temp0[w3+c]]+LT[5][temp0[w3+c+3]]+
                                                        LT[6][temp1[w3+c-3]]+LT[7][temp1[w3+c]]+LT[8][temp1[w3+c+3]]));}

    //BlurV1
    imgout += linesize;
    for(c = 0; c < 3; ++c){imgout[c] = (uint8_t)((      LT[0][temp0[c]]+LT[1][temp0[c]]+LT[2][temp0[c+3]]+
                                                        LT[3][temp1[c]]+LT[4][temp1[c]]+LT[5][temp1[c+3]]+
                                                        LT[6][temp2[c]]+LT[7][temp2[c]]+LT[8][temp2[c+3]]));}
    for(uint32_t i =3; i < w3-3; i+=3){
        for(c = 0; c < 3; ++c){imgout[i + c] = (uint8_t)((      LT[0][temp0[i+c-3]]+LT[1][temp0[i+c]]+LT[2][temp0[i+c+3]]+
                                                                LT[3][temp1[i+c-3]]+LT[4][temp1[i+c]]+LT[5][temp1[i+c+3]]+
                                                                LT[6][temp2[i+c-3]]+LT[7][temp2[i+c]]+LT[8][temp2[i+c+3]]));}
    }
    for(c = 0; c < 3; ++c){imgout[w3 + c] = (uint8_t)(( LT[0][temp0[w3+c-3]]+LT[1][temp0[w3+c]]+LT[2][temp0[w3+c+3]]+
                                                        LT[3][temp1[w3+c-3]]+LT[4][temp1[w3+c]]+LT[5][temp1[w3+c+3]]+
                                                        LT[6][temp2[w3+c-3]]+LT[7][temp2[w3+c]]+LT[8][temp2[w3+c+3]]));}
    imgout += linesize;
    //Mid
    //Blur
	for(uint32_t j = 3; j < h-1; ++j){
        //Move
        swap(temp0,temp1);
        swap(temp1,temp2);
        copy(imgout+linesize,imgout+linesize*2,temp2);
        //BlurV
        for(c = 0; c < 3; ++c){imgout[c] = (uint8_t)((  LT[0][temp0[c]]+LT[1][temp0[c]]+LT[2][temp0[c+3]]+
                                                        LT[3][temp1[c]]+LT[4][temp1[c]]+LT[5][temp1[c+3]]+
                                                        LT[6][temp2[c]]+LT[7][temp2[c]]+LT[8][temp2[c+3]]));}
        for(uint32_t i =3; i < w3-3; i+=3){
                for(c = 0; c < 3; ++c){imgout[i + c] = (uint8_t)((      LT[0][temp0[i+c-3]]+LT[1][temp0[i+c]]+LT[2][temp0[i+c+3]]+
                                                                        LT[3][temp1[i+c-3]]+LT[4][temp1[i+c]]+LT[5][temp1[i+c+3]]+
                                                                        LT[6][temp2[i+c-3]]+LT[7][temp2[i+c]]+LT[8][temp2[i+c+3]]));}
        }
        for(c = 0; c < 3; ++c){imgout[w3 + c] = (uint8_t)((     LT[0][temp0[w3+c-3]]+LT[1][temp0[w3+c]]+LT[2][temp0[w3+c+3]]+
                                                                LT[3][temp1[w3+c-3]]+LT[4][temp1[w3+c]]+LT[5][temp1[w3+c+3]]+
                                                                LT[6][temp2[w3+c-3]]+LT[7][temp2[w3+c]]+LT[8][temp2[w3+c+3]]));}
        imgout += linesize;
    }
    //Bot
    for(c = 0; c < 3; ++c){imgout[c] = (uint8_t)((      LT[0][temp1[c]]+LT[1][temp1[c]]+LT[2][temp1[c+3]]+
                                                        LT[3][temp2[c]]+LT[4][temp2[c]]+LT[5][temp2[c+3]]+
                                                        LT[6][temp2[c]]+LT[7][temp2[c]]+LT[8][temp2[c+3]]));}
    for(uint32_t i =3; i < w3-3; i+=3){
        for(c = 0; c < 3; ++c){imgout[i + c] = (uint8_t)((      LT[0][temp1[i+c-3]]+LT[1][temp1[i+c]]+LT[2][temp1[i+c+3]]+
                                                                LT[3][temp2[i+c-3]]+LT[4][temp2[i+c]]+LT[5][temp2[i+c+3]]+
                                                                LT[6][temp2[i+c-3]]+LT[7][temp2[i+c]]+LT[8][temp2[i+c+3]]));}
    }
    for(c = 0; c < 3; ++c){imgout[w3 + c] = (uint8_t)(( LT[0][temp1[w3+c-3]]+LT[1][temp1[w3+c]]+LT[2][temp1[w3+c+3]]+
                                                        LT[3][temp2[w3+c-3]]+LT[4][temp2[w3+c]]+LT[5][temp2[w3+c+3]]+
                                                        LT[6][temp2[w3+c-3]]+LT[7][temp2[w3+c]]+LT[8][temp2[w3+c+3]]));}
	//Clean
	free(temp);
}
void Transform::matrix3(float mat[9]){
    //Init
    normalize(mat);
    //SVD
    float U[9],S[9],V[9],HV[3],VV[3];
    svd(    mat[0],mat[1],mat[2],mat[3],mat[4],mat[5],mat[6],mat[7],mat[8],
            U[0],U[1],U[2],U[3],U[4],U[5],U[6],U[7],U[8],
            S[0],S[1],S[2],S[3],S[4],S[5],S[6],S[7],S[8],
            V[0],V[1],V[2],V[3],V[4],V[5],V[6],V[7],V[8]);
    VV[0]=V[0];VV[1]=V[3];VV[2]=V[6];
    HV[0]=U[0];HV[1]=U[3];HV[2]=U[6];
    VV[0]*=sqrt(S[0]);VV[1]*=sqrt(S[0]);VV[2]*=sqrt(S[0]);
    HV[0]*=sqrt(S[0]);HV[1]*=sqrt(S[0]);HV[2]*=sqrt(S[0]);
    //VV[0]=abs(VV[0]);VV[1]=abs(VV[1]);VV[2]=abs(VV[2]);
    //HV[0]=abs(HV[0]);HV[1]=abs(HV[1]);HV[2]=abs(HV[2]);
    //
    printMat(mat);
    printMat(U);
    printMat(S);
    printMat(V);
    cout << VV[0] << "\t" << VV[1] << "\t" << VV[2] <<endl;
    cout << HV[0] << "\t" << HV[1] << "\t" << HV[2] <<endl;
    const uint32_t w = image.width, h = image.height,linesize = sizeof(uint8_t)*image.stride;
    const uint32_t w3 = w*3;
    uint32_t c = 0;
    uint8_t *temp  = (uint8_t*)malloc(linesize*4);
    uint8_t *temp0 = temp;
    uint8_t *temp1 = temp0+linesize;
    uint8_t *temp2 = temp1+linesize;
    uint8_t *tempv = temp2+linesize;
    uint8_t *imgout = image.image;
    //LUT
    uint16_t LT1V[256],LT2V[256],LT3V[256];
    uint16_t LT1H[256],LT2H[256],LT3H[256];
    for(uint32_t i = 0; i < 256; ++i){
    	LT1V[i]=(uint16_t)((i<<8) * VV[0]);LT1H[i]=(uint16_t)((i<<8) * HV[0]);
    	LT2V[i]=(uint16_t)((i<<8) * VV[1]);LT2H[i]=(uint16_t)((i<<8) * HV[1]);
    	LT3V[i]=(uint16_t)((i<<8) * VV[2]);LT3H[i]=(uint16_t)((i<<8) * HV[2]);
    }
    //Transform horizontal
    //Top
    copy(imgout,imgout+linesize*3,temp);
    //BlurH0
    for(uint32_t i =0; i < w3; i+=3){
        for(c = 0; c < 3; ++c){tempv[i + c] = (uint8_t)((LT1H[temp0[i+c]]+LT2H[temp0[i+c]]+LT3H[temp1[i+c]])>>8);}
    }
    //BlurV0
    for(c = 0; c < 3; ++c){imgout[c] = (uint8_t)((LT1V[tempv[c]]+LT2V[tempv[c]]+LT3V[tempv[c+3]])>>8);}
    for(uint32_t i =3; i < w3-3; i+=3){
        for(c = 0; c < 3; ++c){imgout[i + c] = (uint8_t)((LT1V[tempv[i+c-3]]+LT2V[tempv[i+c]]+LT3V[tempv[i+c+3]])>>8);}
    }
    for(c = 0; c < 3; ++c){imgout[w3 + c] = (uint8_t)((LT1V[tempv[w3+c-3]]+LT2V[tempv[w3+c]]+LT3V[tempv[w3+c]])>>8);}
    //BlurH1
    for(uint32_t i =0; i < w3; i+=3){
        for(c = 0; c < 3; ++c){tempv[i + c] = (uint8_t)((LT1H[temp0[i+c]]+LT2H[temp1[i+c]]+LT3H[temp2[i+c]])>>8);}
    }
    //BlurV1
    imgout += linesize;
    for(c = 0; c < 3; ++c){imgout[c] = (uint8_t)((LT1V[tempv[c]]+LT2V[tempv[c]]+LT3V[tempv[c+3]])>>8);}
    for(uint32_t i =3; i < w3-3; i+=3){
        for(c = 0; c < 3; ++c){imgout[i + c] = (uint8_t)((LT1V[tempv[i+c-3]]+LT2V[tempv[i+c]]+LT3V[tempv[i+c+3]])>>8);}
    }
    for(c = 0; c < 3; ++c){imgout[w3 + c] = (uint8_t)((LT1V[tempv[w3+c-3]]+LT2V[tempv[w3+c]]+LT3V[tempv[w3+c]])>>8);}
    imgout += linesize;
    //Mid
    //Blur
	for(uint32_t j = 3; j < h-1; ++j){
        //Move
        swap(temp0,temp1);
        swap(temp1,temp2);
        copy(imgout+linesize,imgout+linesize*2,temp2);
        //BlurH
        for(uint32_t i =0; i < w3; i+=3){
            for(c = 0; c < 3; ++c){tempv[i + c] = (uint8_t)((LT1H[temp0[i+c]]+LT2H[temp1[i+c]]+LT3H[temp2[i+c]])>>8);}
        }
        //BlurV
        for(c = 0; c < 3; ++c){imgout[c] = (uint8_t)((LT1V[tempv[c]]+LT2V[tempv[c]]+LT3V[tempv[c+3]])>>8);}
        for(uint32_t i =3; i < w3-3; i+=3){
            for(c = 0; c < 3; ++c){
                imgout[i + c] = (uint8_t)((LT1V[tempv[i+c-3]]+LT2V[tempv[i+c]]+LT3V[tempv[i+c+3]])>>8);
            }
        }
        for(c = 0; c < 3; ++c){imgout[w3 + c] = (uint8_t)((LT1V[tempv[w3+c-3]]+LT2V[tempv[w3+c]]+LT3V[tempv[w3+c]])>>8);}
        imgout += linesize;
    }
    //Bot
    //BlurH+1
    for(uint32_t i = 0; i < w3; i+=3){
        for(c = 0; c < 3; ++c){tempv[i + c] = (uint8_t)((LT1H[temp1[i+c]]+LT2H[temp2[i+c]]+LT3H[temp2[i+c]])>>8);}
    }
    //BlurV
    for(c = 0; c < 3; ++c){imgout[c] = (uint8_t)((LT1V[tempv[c]]+LT2V[tempv[c]]+LT3V[tempv[c+3]])>>8);}
    for(uint32_t i =3; i < w3-3; i+=3){
        for(c = 0; c < 3; ++c){imgout[i + c] = (uint8_t)((LT1V[tempv[i+c-3]]+LT2V[tempv[i+c]]+LT3V[tempv[i+c+3]])>>8);}
    }
    for(c = 0; c < 3; ++c){imgout[w3 + c] = (uint8_t)((LT1V[tempv[w3+c-3]]+LT2V[tempv[w3+c]]+LT3V[tempv[w3+c]])>>8);}
	//Clean
	free(temp);
}
void Transform::printMat(float mat[9]){
        cout << "-----------------" << endl;
        cout << mat[0] << "\t" << mat[1]<< "\t" << mat[2] << endl;
        cout << mat[3] << "\t" << mat[4]<< "\t" << mat[5] << endl;
        cout << mat[6] << "\t" << mat[7]<< "\t" << mat[8] << endl; 
        cout << "-----------------" << endl;
}
void Transform::normalize(float mat[9]){
        float inv = 0;
        for(uint32_t j = 0; j < 9; ++j){inv += mat[j];}
        for(uint32_t j = 0; j < 9; ++j){mat[j]/=inv;}
}