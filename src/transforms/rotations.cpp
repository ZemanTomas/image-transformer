#include "transform.hpp"
void Transform::rr(){//TODO USE STRIDE
        //Init
	int32_t w = image.width, h = image.height;
	uint8_t* temp = (uint8_t*)malloc(sizeof(uint8_t)*image.height*image.stride);
        //Transform
        for(int32_t i = 0; i < h; ++i){
                int32_t tc = (h-i-1)*3,
                        br = i*w*3;
        	for(int32_t j = 0; j < w; ++j){
                        int32_t tr = j*h*3,
                                bc = j*3;
                        temp[tr + tc + 0] = image.image[br + bc + 0];
                        temp[tr + tc + 1] = image.image[br + bc + 1];
                        temp[tr + tc + 2] = image.image[br + bc + 2];
        }}
        //Clean
	free(image.image);
        image.image = temp;
        image.width = h;
        image.height = w;
        image.stride = image.width*3;
}

void Transform::rl(){
        //Init
	int32_t w = image.width, h = image.height;
	uint8_t* temp = (uint8_t*)malloc(sizeof(uint8_t)*image.height*image.stride);
        //Transform
        for(int32_t i = 0; i < h; ++i){
                int32_t tc = i*3,
                        br = i*w*3;
                for(int32_t j = 0; j < w; ++j){
                        int32_t tr = (w-j-1)*h*3,
                                bc = j*3;
                        temp[tr + tc + 0] = image.image[br + bc + 0];
                        temp[tr + tc + 1] = image.image[br + bc + 1];
                        temp[tr + tc + 2] = image.image[br + bc + 2];
        }}
        //Clean
	free(image.image);
        image.image = temp;
        image.width = h;
        image.height = w;
        image.stride = image.width*3;
}

void Transform::fh(){
	//Init
	int32_t w = image.width, h = image.height;
	uint8_t* temp = (uint8_t*)malloc(sizeof(uint8_t)*image.height*image.stride);
        //Transform
        for(int32_t i = 0; i < h; ++i){
                int32_t tr = i*w*3,
                        br = tr;
                for(int32_t j = 0; j < w; ++j){
                        int32_t tc = (w-j-1)*3,
                                bc = j*3;
                        temp[tr + tc + 0] = image.image[br + bc + 0];
                        temp[tr + tc + 1] = image.image[br + bc + 1];
                        temp[tr + tc + 2] = image.image[br + bc + 2];
        }}
	//Clean
	free(image.image);
        image.image = temp;
}

void Transform::fv(){
        //Init
	int32_t w = image.width, h = image.height;
	uint8_t* temp = (uint8_t*)malloc(sizeof(uint8_t)*image.height*image.stride);
        //Transform
        for(int32_t i = 0; i < h; ++i){
                int32_t tr = (h-i-1)*w*3,
                        br = i*w*3;
                for(int32_t j = 0; j < w; ++j){
                        int32_t tc = j*3,
                                bc = tc;
                        temp[tr + tc + 0] = image.image[br + bc + 0];
                        temp[tr + tc + 1] = image.image[br + bc + 1];
                        temp[tr + tc + 2] = image.image[br + bc + 2];
        }}
        //Clean
	free(image.image);
        image.image = temp;
}