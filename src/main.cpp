#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <stdlib.h>
#include <string>

#include "transform.hpp"
#include "getopt.hpp"

#define VERSION "V0.8"
#define DEFAULT_QUALITY 100
//MAIN
using namespace std;

void displayHelp();

int main(int argc, char** argv) {
	//This does not work nor help:
	//ios_base::sync_with_stdio(false);
	//Parse arguments
	argParser parser = argParser(argc,argv);
	parser.addOptionLong("help",false,'h');
	parser.addOptionLong("version",false,'v');
	parser.addOptionLong("input",true,'i');
	parser.addOptionLong("output",true,'o');
	parser.addOptionLong("transform",true,'t');
	parser.addOptionLong("quality",true,'q');
	parser.eval();
	if(parser.isSet("help")){displayHelp();return 0;}
	if(parser.isSet("version")){cerr << VERSION << endl;return 0;}
	if(!parser.isSet("input") || !parser.isSet("output")){displayHelp();return 0;}

	//Initialize variables
	string input = parser.get("input");
	string output = parser.get("output");
	string param;
	if(parser.isSet("transform")){param = parser.get("transform");}
	uint8_t quality = parser.isSet("quality")?(uint8_t)min((int32_t)stoul(parser.get("quality")),DEFAULT_QUALITY):DEFAULT_QUALITY;

	//Read input
	Transform tr(input);

	//Transform
	tr.transform(param);

	//Write to output
	if(tr.write(output,quality)){
		cerr << "Error, probably bad output extension." << endl;
	}

	return 0;
}

void displayHelp(){
	cerr << "Usage: ./transform -i <input image> -o <output image> [-t <transformations>] [-q <quality>]" << endl;
}
