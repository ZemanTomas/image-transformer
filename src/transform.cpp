#include <math.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <iterator>
#include <climits>
#include <vector>

#include "transform.hpp"

using namespace std;
using namespace mango;

Transform::Transform(string filename) :
        image{filename,FORMAT_R8G8B8}
{}

void Transform::transform(string param){
        stringstream paramss(param);
        istream_iterator<string> begin(paramss);
        istream_iterator<string> end;
        vector<string> paramv(begin, end);
for(size_t i = 0; i < paramv.size(); ){
        string arg(paramv[i++]);
        float mat[9];
        if(arg.compare("rr") == 0){rr();}else
        if(arg.compare("rl") == 0){rl();}else
        if(arg.compare("fh") == 0){fh();}else
        if(arg.compare("fv") == 0){fv();}else
        if(arg.compare("con") == 0){
                int32_t c = stoi(paramv[i++]);
                if(c == 0){continue;}
                con(c);
	}else
        if(arg.compare("crop") == 0){
                int32_t w = stoi(paramv[i++]);
                int32_t h = stoi(paramv[i++]);
                int32_t x = stoi(paramv[i++]);
                int32_t y = stoi(paramv[i++]);
                crop(w,h,x,y);
        }else
        if(arg.compare("col") == 0){
                float r = stof(paramv[i++])+1.0f;
                float g = stof(paramv[i++])+1.0f;
                float b = stof(paramv[i++])+1.0f;
                if(abs(1.0-r)<0.01 && abs(1.0-g)<0.01 && abs(1.0-b)<0.01){continue;}
                col(r,g,b);
        }else
        if(arg.compare("res") == 0){
                int32_t nw = stoi(paramv[i++]);
                int32_t nh = stoi(paramv[i++]);
                if((nw <= 0 && nh <= 0) || (nw==image.width && nh==image.height ) || (nh*nw == 0)){continue;}
                res(nw,nh);
        }else
	if(arg.compare("matrix") == 0 || arg.compare("mat") == 0){//Best
                getMat(mat,paramv,i);
                bool isNegative = false;
                for(uint32_t j = 0; j < 9; ++j){if(mat[j] < 0){isNegative = true;}}
                if(isNegative){
                        matrix2(mat);
                }else{
                        matrix3(mat);
                }
        }else
	if(arg.compare("matrix1") == 0 || arg.compare("mat1") == 0){ //basic
                getMat(mat,paramv,i);
                matrix(mat);
        }else
	if(arg.compare("matrix2") == 0 || arg.compare("mat2") == 0){ //cache
                getMat(mat,paramv,i);
                matrix2(mat);
        }else
	if(arg.compare("matrix3") == 0 || arg.compare("mat3") == 0){ //SVD + cache
                getMat(mat,paramv,i);
                matrix3(mat);
        }else
        if(arg.compare("gauss") == 0){gauss5();}else //Best
        if(arg.compare("gauss1") == 0){gauss1();}else //basic
        if(arg.compare("gauss1+") == 0){gauss1p();}else//basic+3LUT
        if(arg.compare("gauss2") == 0){gauss2();}else //2 pass+2LUT
        if(arg.compare("gauss3") == 0){gauss3();}else//box blur H+T
        if(arg.compare("gauss4") == 0){gauss4();}else//box blur H+T accumulator
        if(arg.compare("gauss5") == 0){gauss5();}//2 pass sequential + 2LUT + cache
}
}

int Transform::write(string filename, uint8_t quality){
        ImageEncodeOptions opt;
        opt.quality = (float)quality / 100.0f;
        opt.dithering = false;
        if(quality == 100){opt.lossless = true; opt.quality = 1.0f;}

        //This is just image.save(filename) :
        ImageEncoder encoder(filename);
        if (encoder.isEncoder())
        {
                filesystem::FileStream file(filename, Stream::WRITE);
                encoder.encode(file, image, opt);
                return 0;
        }else{
                return 1;
        }
}

void Transform::getpix(int32_t x, int32_t y,uint8_t tmp[3]){
	x = min(image.width-1,max(0,x));
        y = min(image.height-1,max(0,y));
	tmp[0] = image.image[y*image.stride + x*3 + 0];
	tmp[1] = image.image[y*image.stride + x*3 + 1];
	tmp[2] = image.image[y*image.stride + x*3 + 2];
}

void Transform::getpix_unsafe(int32_t x, int32_t y,uint8_t tmp[3]){
	tmp[0] = image.image[y*image.stride + x*3 + 0];
	tmp[1] = image.image[y*image.stride + x*3 + 1];
	tmp[2] = image.image[y*image.stride + x*3 + 2];
}
void Transform::getMat(float mat[9], string paramv, size_t &i){
        mat[0] = stof(paramv[i++]);mat[1] = stof(paramv[i++]);mat[2] = stof(paramv[i++]);
        mat[3] = stof(paramv[i++]);mat[4] = stof(paramv[i++]);mat[5] = stof(paramv[i++]);
        mat[6] = stof(paramv[i++]);mat[7] = stof(paramv[i++]);mat[8] = stof(paramv[i++]);
}

#include "transforms/rotations.cpp"
#include "transforms/contrast.cpp"
#include "transforms/crop.cpp"
#include "transforms/colours.cpp"
#include "transforms/resize.cpp"
#include "transforms/matrix.cpp"
#include "transforms/gauss.cpp"