#pragma once
#include <string>
#include <cstdint>

#include <mango/image/image.hpp>

using namespace std;

class Transform {
public:
Transform(string);
mango::Bitmap image;
void transform(string);
int write(string,uint8_t);
private:
//int32_t size;
void rr();
void rl();
void fv();
void fh();
void con(int32_t con);
void crop(int32_t new_w, int32_t new_h, int32_t x, int32_t y);
void col(float r, float g, float b);

void res(int32_t nw, int32_t nh);
void bicubic(float u, float v, uint8_t *buf);
float herm(float a,float b,float c,float d, float t);

void matrix(float mat[9]);
void matrix2(float mat[9]);
void matrix3(float mat[9]);
void printMat(float mat[9]);
void normalize(float mat[9]);

void gauss1();
void gauss1p();
void gauss2();

void gauss3();
void gaussBoxH(uint8_t *temp);
void gaussBoxT(uint8_t *temp);

void gauss4();
void gaussBoxHA(uint8_t *temp, uint8_t LT[766]);
void gaussBoxTA(uint8_t *temp, uint8_t LT[766]);

void gauss5();

void getpix(int32_t x, int32_t y,uint8_t tmp[3]);
void getpix_unsafe(int32_t x, int32_t y,uint8_t tmp[3]);
void getMat(float mat[9], string paramv, size_t &i);

};
