#!/bin/bash
if [ $# -ne 1 ] && [ $# -ne 2 ]; then echo "Usage: ./benchmark.sh filename [repeats]"; exit 1 ; fi
#DECLARATIONS
VERSION="V01.9"
declare -a BM
declare -a QL
declare -a DESC
declare -a DATESTART
declare -a DATESTOP
declare -a DATESUM

REPEATS=${2:-10}
BM[${#BM[@]}]=""									;QL[${#QL[@]}]="100"	;DESC[${#DESC[@]}]="EMPTY,100"
BM[${#BM[@]}]=""									;QL[${#QL[@]}]="70"	;DESC[${#DESC[@]}]="EMPTY,070"
BM[${#BM[@]}]=""									;QL[${#QL[@]}]="40"	;DESC[${#DESC[@]}]="EMPTY,040"
BM[${#BM[@]}]="rl"									;QL[${#QL[@]}]="100"	;DESC[${#DESC[@]}]="RLEFT,100"
BM[${#BM[@]}]="fh"									;QL[${#QL[@]}]="100"	;DESC[${#DESC[@]}]="FLIPH,100"
BM[${#BM[@]}]="matrix 0 0 0 0 1 0 0 0 0"						;QL[${#QL[@]}]="100"	;DESC[${#DESC[@]}]="IDENT,100"
BM[${#BM[@]}]="matrix 0 -0.5 0 -0.5 3 -0.5 0 -0.5 0"					;QL[${#QL[@]}]="100"	;DESC[${#DESC[@]}]="SHARP,100"
BM[${#BM[@]}]="matrix 0.0778 0.1233 0.0778 0.1233 0.1953 0.1233 0.0778 0.1233 0.0778"	;QL[${#QL[@]}]="100"	;DESC[${#DESC[@]}]="MATG1,100"
BM[${#BM[@]}]="con 100"									;QL[${#QL[@]}]="100"	;DESC[${#DESC[@]}]="CONTR,100"
BM[${#BM[@]}]="col 0.2 -0.3 0.4"							;QL[${#QL[@]}]="100"	;DESC[${#DESC[@]}]="COLOR,100"
BM[${#BM[@]}]="gauss1"									;QL[${#QL[@]}]="100"	;DESC[${#DESC[@]}]="GAUS1,100"
BM[${#BM[@]}]="gauss1+"									;QL[${#QL[@]}]="100"	;DESC[${#DESC[@]}]="GAUS+,100"
BM[${#BM[@]}]="gauss2"									;QL[${#QL[@]}]="100"	;DESC[${#DESC[@]}]="GAUS2,100"
BM[${#BM[@]}]="gauss3"									;QL[${#QL[@]}]="100"	;DESC[${#DESC[@]}]="GAUS3,100"
BM[${#BM[@]}]="gauss4"									;QL[${#QL[@]}]="100"	;DESC[${#DESC[@]}]="GAUS4,100"
BM[${#BM[@]}]="gauss5"									;QL[${#QL[@]}]="100"	;DESC[${#DESC[@]}]="GAUS5,100"
BM[${#BM[@]}]="matrix2 0.0778 0.1233 0.0778 0.1233 0.1953 0.1233 0.0778 0.1233 0.0778"  ;QL[${#QL[@]}]="100"	;DESC[${#DESC[@]}]="MATG2,100"
BM[${#BM[@]}]="matrix3 0.0778 0.1233 0.0778 0.1233 0.1953 0.1233 0.0778 0.1233 0.0778"  ;QL[${#QL[@]}]="100"	;DESC[${#DESC[@]}]="MATG3,100"

#EXECUTION
for (( i=0; i<${#BM[@]}; i++ )); do
	for (( j=0; j<${REPEATS}; j++ )); do
	DATESTART[${i}]=$(date +%s%N)
		./transform -i ${1} -o ${1}.${i}.benchmarked.jpeg -t "${BM[${i}]}" -q ${QL[${i}]} 2>/dev/null 1>/dev/null
	DATESTOP[${i}]=$(date +%s%N)
	((DATESUM[${i}] += (${DATESTOP[${i}]} - ${DATESTART[${i}]} )))
	done
	#COMMENT HERE FOR KEEPING FILES
	rm ${1}.${i}.benchmarked.jpeg 2>/dev/null
done
#OUTPUT
printf "REPEATS: %d\n" ${REPEATS}
OUT=$(printf "%s|%6.6d" ${VERSION} ${REPEATS})
for (( i=0; i<${#BM[@]}; i++ )); do
printf  "%s:%6.6d\n" ${DESC[${i}]} $(( ${DATESUM[${i}]} / (1000000*${REPEATS}) ))
OUT=$(printf "%s|%6.6d" ${OUT} $((${DATESUM[${i}]}/(1000000*${REPEATS}))))
done
TOTAL=0
for (( i=0; i<${#BM[@]}; i++ )); do
(( TOTAL += ( ${DATESUM[${i}]} / (1000000*${REPEATS})) ))
done
printf "TOTAL TIME: %d\n" ${TOTAL}
printf "%s|%6.6d\n" ${OUT} ${TOTAL}
