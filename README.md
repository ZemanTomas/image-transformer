# Image Transformer

Image manipulation tool focused on performance over correctness

## INSTALL:

init submodules

install libs/mango (or add path to .so to Makefile)

execute build.sh (should build transform binary from src/)

check that img/ directory exists and permissions are correct (index.php executes transform)

install web server and php

index.php is the main page

structure:
```
.
├── CHANGELOG
├── README.md
├── transform
├── index.php
├── Makefile
├── img
│   └── <your folders/images>
├── js
│   └── main.js
├── css
│   └── main.css
├── libs
│   ├── <additional libraries may be here>
│   ├── mango
│   ├── svd3
│   └── getoptcpp <git@gitlab.com:ZemanTomas/getoptcpp.git>
├── src
│   ├── transforms
│   │   ├── rotations.cpp
│   │   ├── contrast.cpp
│   │   ├── crop.cpp
│   │   ├── colours.cpp
│   │   ├── gauss.cpp
│   │   ├── resize.cpp
│   │   └── matrix.cpp
│   ├── image.hpp
│   ├── main.cpp
│   ├── transform.cpp
│   └── transform.hpp
└── benchmark.sh
```
## DEVELOPERS:

index.php is the main script

index.php takes care of image uploads (POST form), does most checks (image size, image extension, mimetype, transform string), deletes files (POST form) previews image (GET parameter) and executes transforming (POST form)

program execution structure:
main.cpp:
1.parameter check
2.create Transform object -> Constructor loads and converts image to RGB 8-bit 1-D array
3.call transform on Transform object -> parses string and sequentially transform the image stored
4.call write on Transform object -> compresses image and saves to disk

adding new transformations is easy, only steps:
1. add function declaration to transform.hpp
2. add file with implementation to transforms/
3. add parameter check to transform.cpp and include implementation 
4. add string check to index.php function checkTransform() and add parameters handling to transform function
5. add menuitem form content to function getMenuBody() in index.php
OPTIONAL:
6. add line to benchmark.sh

## BENCHMARKING:

execute benchmark.sh <image> [<repeats>]

last line sums all the information

## ISSUES:

on some browsers, file upload does not work (try updating it), the dialog window won't open

firefox does not support decoding certaing bmp formats, like the one used by mango library :(

I do not suggest uploading files with spaces (or worse characters) in filename (nor creating such files in the first place)

all files are global, since this tool is not focused on multi-user environment and this type of security is not a priority, but there is an option to add dir=<directory> to get parameters which will create img/<directory> and store/load files in there

Also, true gaussian blur is much smoother than the box blur version : http://www.arannolan.com/documents/gaussian-blur.pdf

matrix3 using svd is not yet implemented for matrices containing negative numbers

due to rounding issues with some matrix-based transformations, the resulting images may be slightly darker

## CREDITS:

mango library: https://github.com/t0rakka/mango

bicubic resampling algorithm: https://blog.demofox.org/2015/08/15/resizing-images-with-bicubic-interpolation/

fast 3x3 SVD decomposition implementation: https://github.com/ericjang/svd3

gaussian kernel constants from: http://dev.theomader.com/gaussian-kernel-calculator/

## TODO:

Update changelog

Intrinsics -> avx instructions

general gaussian blur nxn

cache specific algorithms

multithreaded convolution

batch processing

