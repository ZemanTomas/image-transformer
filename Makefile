CXX=g++
SRCDIR=src
LIBDIR=libs
CXXFLAGS= -Wall -Wextra -Wpedantic -Ofast -pipe -march=native -std=c++17 -fno-trapping-math -fno-signed-zeros -frename-registers -funroll-loops -fprefetch-loop-arrays
INCLUDE= -I$(LIBDIR)/getoptcpp -I$(LIBDIR)/svd3 -I$(SRCDIR)
all: transform

transform: $(SRCDIR)/transform.o $(SRCDIR)/main.o $(SRCDIR)/getopt.o
	$(CXX) $(CXXFLAGS) $(INCLUDE) $(SRCDIR)/transform.o $(SRCDIR)/main.o $(SRCDIR)/getopt.o -o transform -lmango -ldl

$(SRCDIR)/transform.o: $(SRCDIR)/transform.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $(SRCDIR)/transform.cpp -o $(SRCDIR)/transform.o -lmango -ldl

$(SRCDIR)/main.o: $(SRCDIR)/main.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $(SRCDIR)/main.cpp -o $(SRCDIR)/main.o -lmango -ldl

$(SRCDIR)/getopt.o: $(LIBDIR)/getoptcpp/getopt.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $(LIBDIR)/getoptcpp/getopt.cpp -o $(SRCDIR)/getopt.o

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $^ -o $@ -lmango -ldl

clean:
	rm $(SRCDIR)/*.o
